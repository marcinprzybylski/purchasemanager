import { Injectable } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';

export class SignalrWindow extends Window {  
  $: any;
}

export enum ConnectionState {  
  Connecting = 1,
  Connected = 2,
  Reconnecting = 3,
  Disconnected = 4
}

export class ChannelConfig {  
  url: string;
  hubName: string;
  channel: string;
}

export class ChannelEvent {  
  Name: string;
  ChannelName: string;
  Timestamp: Date;
  Data: any;
  Json: string;

  constructor() {
      this.Timestamp = new Date();
  }
}

class ChannelSubject {  
  channel: string;
  subject: Subject<ChannelEvent>;
}

@Injectable({
  providedIn: 'root'
})
export class SignalrService {

  starting$: Observable<any>;
  connectionState$: Observable<ConnectionState>;
  error$: Observable<string>;

  pluginUploadObservable$: Observable<any>;

  private connectionStateSubject = new BehaviorSubject<ConnectionState>(ConnectionState.Disconnected);
  private startingSubject = new Subject<any>();
  private errorSubject = new Subject<any>();

  private hubConnection: HubConnection;

  private subjects = new Array<ChannelSubject>();

  constructor(
    private window: SignalrWindow,
    private channelConfig: ChannelConfig
  ) {

    this.connectionState$ = this.connectionStateSubject.asObservable();
    this.error$ = this.errorSubject.asObservable();
    this.starting$ = this.startingSubject.asObservable();

    this.hubConnection = new HubConnectionBuilder()
      .withUrl(channelConfig.url)
      .build();

    // this.hubConnection.on('OnPluginUploaded', (data) => {
    //   console.log(data);
    // });

    this.pluginUploadObservable$ = Observable
      .create(observer => {
        this.hubConnection
          .on('OnPluginUpload', message => observer.next(message))
      })

    this.hubConnection.onclose(async () => {
      await this.start();
    })
  }
  
  start(): void {
    this.hubConnection.start()
      .then(() => {
        this.startingSubject.next();
      })
      .catch((error: any) => {
        this.errorSubject.next(error);
        this.startingSubject.error(error);
      });
  }

  sub(channel: string): Observable<ChannelEvent> {

    let channelSub = this.subjects.find((x: ChannelSubject) => {
      return x.channel === channel;
    }) as ChannelSubject;

    if(channelSub !== undefined) {
      console.log(`Found existing obervable for ${channel} channel`);
      return channelSub.subject.asObservable();
    }

    channelSub = new ChannelSubject();
    channelSub.channel = channel;
    channelSub.subject = new Subject<ChannelEvent>();
    this.subjects.push(channelSub);

    this.starting$.subscribe(() => {
      this.hubConnection.invoke("Subscribe", channel)
        .then(() => {
          console.log(`Successfully subscribed to ${channel} channel`);
        })
        .catch((error: any) => {
          channelSub.subject.error(error);
        });
    },
    (error: any) => {
      channelSub.subject.error(error);
    });

    return channelSub.subject.asObservable();
  }

  publish(ev: ChannelEvent): void {
    this.hubConnection.invoke("Publish", ev);
  }
}
