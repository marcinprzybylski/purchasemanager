import { SignalrService, ConnectionState } from './signalr/signalr.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  template: `
      <div>
          <h3>SignalR w/ Angular 2 demo</h3>
      </div>

      <div>
          <span>Connection state: {{connectionState$ | async}}</span>
      </div>

      <div class="flex-row">
          <task class="flex"
              [eventName]="'pluginUploaded'"
              [apiUrl]="'http://localhost:5000/tasks/plugin'"></task>
      </div>
   `
})
export class AppComponent implements OnInit {
  title = 'app';

  connectionState$: Observable<string>;
  
  constructor(private signalrService: SignalrService) {

    this.connectionState$ = this.signalrService.connectionState$
      .pipe(
        map((state: ConnectionState) => {return ConnectionState[state]; })
      );

    this.signalrService.starting$.subscribe(
      () => { console.log("signalr service has been started"); },
      () => { console.log("signalr service failed to start!"); }
    );

    this.signalrService.pluginUploadObservable$.subscribe(
      (data) => {console.log(data);}
    );
  }

  ngOnInit() {
    // Start the connection up!
    //
    console.log("Starting the channel service");

    this.signalrService.start();
  }
}
