import { EnvServiceProvider } from './env.service.provider';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TaskComponent } from './task/task.component';
import { ChannelConfig, SignalrService, SignalrWindow } from './signalr/signalr.service';
import { HttpModule } from '@angular/http';

let channelConfig = new ChannelConfig();
channelConfig.url = "http://localhost:5000/notifications";
channelConfig.hubName = "EventHub";

@NgModule({
  declarations: [
    AppComponent,
    TaskComponent
  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  providers: [
    {provide: SignalrWindow, useValue: window},
    {provide: ChannelConfig, useValue: channelConfig},
    EnvServiceProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }