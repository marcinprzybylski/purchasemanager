﻿using Moq;
using PurchaseManager.Api.Ngrok;
using PurchaseManager.Tests.Data;
using System.Threading.Tasks;
using Xunit;

namespace PurchaseManager.Tests
{
    public class NgrokTunnelsProvider_Tests
    {
        [Fact]
        public async Task WhenGetNgrokTunnels_Called_ReturnsList()
        {
            var httpClient = new Mock<INgrokHttpClient>();
            httpClient.Setup(client => client.GetTunnelsInfoStream())
                .ReturnsAsync(DataProvider.GetStream(Constants.NgrokTunnelsInfoJson));
            var ngrokTunnelsProvider = new NgrokTunnelsProvider(httpClient.Object);

            var ngrokTunnels = await ngrokTunnelsProvider.GetNgrokTunnels();

            Assert.NotEmpty(ngrokTunnels.Tunnels);
        }
    }
}
