﻿namespace PurchaseManager.Tests.Data
{
    internal class Constants
    {
        internal const string AppVeyorDefaultPayloadJson = "appVeyorDefaultPayload.json";
        internal const string NgrokTunnelsInfoJson = "ngrokTunnelsInfo.json";
    }
}
