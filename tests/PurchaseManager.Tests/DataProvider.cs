﻿using System.IO;
using System.Reflection;
using PurchaseManager.Tests.Data;

namespace PurchaseManager.Tests
{
    public class DataProvider
    {
        public static Stream GetStream(string embeddedTestFileName)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceStream = assembly.GetManifestResourceStream("PurchaseManager.Tests.Data." + embeddedTestFileName);

            return resourceStream;
        }
    }
}
