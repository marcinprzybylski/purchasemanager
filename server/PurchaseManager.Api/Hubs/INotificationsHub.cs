﻿using System.Threading.Tasks;

namespace PurchaseManager.Api.Hubs
{
    public interface INotificationsHub
    {
        Task OnEvent(string channelName, ChannelEvent channelEvent);
        Task NotifyAboutNewBuild(dynamic buildData);
        Task OnPluginUploaded(PluginUploadedEvent pluginUploadedEvent);
    }
}