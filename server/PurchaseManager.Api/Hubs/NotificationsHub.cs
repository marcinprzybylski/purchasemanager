﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;

namespace PurchaseManager.Api.Hubs
{
    public class NotificationsHub : Hub<INotificationsHub>
    {
        public async Task Subscribe(string channel)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, channel);

            var ev = new ChannelEvent
            {
                ChannelName = "Admin.Channel",
                Name = "user.subscribed",
                Data = new
                {
                    Context.ConnectionId,
                    ChannelName = channel
                }
            };

            await Publish(ev);
        }

        public async Task Unsubscribe(string channel)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, channel);

            var ev = new ChannelEvent
            {
                ChannelName = "Admin.Channel",
                Name = "user.unsubscribed",
                Data = new
                {
                    Context.ConnectionId,
                    ChannelName = channel
                }
            };

            await Publish(ev);
        }

        public Task Publish(ChannelEvent channelEvent)
        {
            Clients.Group(channelEvent.ChannelName).OnEvent(channelEvent.ChannelName, channelEvent);

            if (channelEvent.ChannelName != "Admin.Channel")
            {
                Clients.Group("Admin.Channel").OnEvent("Admin.Channel", channelEvent); 
            }

            return Task.FromResult(0);
        }

        public override Task OnConnectedAsync()
        {
            var ev = new ChannelEvent
            {
                ChannelName = "Admin.Channel",
                Name = "user.connected",
                Data = new
                {
                    Context.ConnectionId
                }
            };

            Publish(ev);

            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            var ev = new ChannelEvent
            {
                ChannelName = "Admin.Channel",
                Name = "user.disconnected",
                Data = new
                {
                    Context.ConnectionId
                }
            };

            Publish(ev);

            return base.OnDisconnectedAsync(exception);
        }

        public void Notify()
        {
            this.Clients.Caller.NotifyAboutNewBuild(new { Message = "Hello" });
        }
    }
}
