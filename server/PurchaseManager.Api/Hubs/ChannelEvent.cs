﻿using Newtonsoft.Json;
using System;
using System.Reflection;

namespace PurchaseManager.Api.Hubs
{
    public class ChannelEvent
    {
        public string Name { get; set; }

        public string ChannelName { get; set; }

        public DateTimeOffset Timestamp { get; set; }

        public object Data
        {
            get { return _data; }

            set
            {
                _data = value;
                this.Json = JsonConvert.SerializeObject(_data);
            }
        }

        private object _data;

        /// <summary>
        /// A JSON representation of the event data. This is set automatically
        /// when the Data property is assigned.
        /// </summary>
        public string Json { get; private set; }

        public ChannelEvent()
        {
            Timestamp = DateTimeOffset.Now;
        }
    }

    public class PluginUploadedEvent
    {
        public string Name { get; }

        public string Version { get; }

        public PluginUploadedEvent(AssemblyName assemblyName)
        {
            Name = assemblyName.Name;
            Version = assemblyName.Version.ToString();
        }
    }
}