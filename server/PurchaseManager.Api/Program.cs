﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using System;

namespace PurchaseManager.Api
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "PurchaseManager.Api";

            var host = WebHost
                .CreateDefaultBuilder<Startup>(args)
                .Build();

            host.Run();
        }
    }
}
