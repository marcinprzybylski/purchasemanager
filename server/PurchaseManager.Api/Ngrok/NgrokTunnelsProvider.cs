﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.IO;
using System.Threading.Tasks;

namespace PurchaseManager.Api.Ngrok
{
    public class NgrokTunnelsProvider
    {
        private readonly INgrokHttpClient _ngrokHttpClient;

        public NgrokTunnelsProvider(INgrokHttpClient ngrokHttpClient)
        {
            _ngrokHttpClient = ngrokHttpClient;
        }

        public async Task<NgrokTunnelsInfo> GetNgrokTunnels()
        {
            var result = await _ngrokHttpClient.GetTunnelsInfoStream();
            
            using (var sr = new StreamReader(result))
            using (var jsonTextReader = new JsonTextReader(sr))
            {
                var serializer = new JsonSerializer();
                serializer.ContractResolver = new DefaultContractResolver
                {
                    NamingStrategy = new SnakeCaseNamingStrategy()
                };
                var ngrokTunnelInfo = serializer.Deserialize<NgrokTunnelsInfo>(jsonTextReader);
                return ngrokTunnelInfo;
            }
        }
    }
}
