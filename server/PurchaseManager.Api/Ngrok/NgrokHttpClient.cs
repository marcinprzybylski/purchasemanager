﻿using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace PurchaseManager.Api.Ngrok
{
    public class NgrokHttpClient : INgrokHttpClient
    {
        private readonly HttpClient _client = new HttpClient();

        public async Task<Stream> GetTunnelsInfoStream()
        {
            return await _client.GetStreamAsync(Constants.NgrokTunnelsApi);
        }
    }

    public interface INgrokHttpClient
    {
        Task<Stream> GetTunnelsInfoStream();
    }
}
