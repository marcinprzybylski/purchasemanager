﻿using System.Collections.Generic;

namespace PurchaseManager.Api.Ngrok
{
    public class NgrokTunnelsInfo
    {
        public List<NgrokTunnel> Tunnels { get; set; }
    }

    public class NgrokTunnel
    {
        public string Name { get; set; }
        public string Uri { get; set; }
        public string PublicUrl { get; set; }
        public string Proto { get; set; }
        public NgrokTunnelConfig Config { get; set; }
        public NgrokTunnelMetrics Metrics { get; set; }
    }

    public class NgrokTunnelConfig
    {
        public string Addr { get; set; }
        public bool Inspect { get; set; }
    }

    public class NgrokTunnelMetrics
    {

    }
}
