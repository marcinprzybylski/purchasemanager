﻿namespace PurchaseManager.Api
{
    public class Constants
    {
        public const string NgrokTunnelsApi = @"http://127.0.0.1:4040/api/tunnels";
        public const string AppVeyorWebhook = @"https://ci.appveyor.com/api/bitbucket/webhook?id=3x3u2303g0dy8fjg";
    }
}
