﻿namespace PurchaseManager.Api.Watchers
{
    using System.Reactive.Linq;
    using System.IO;
    using System;
    using Microsoft.Extensions.Hosting;
    using System.Threading.Tasks;
    using System.Threading;
    using Microsoft.AspNetCore.SignalR;
    using PurchaseManager.Api.Hubs;
    using System.Reflection;

    public class PluginWatcher : BackgroundService
    {
        private readonly FileSystemWatcher _watcher;
        private readonly IHubContext<NotificationsHub, INotificationsHub> _hubContext;

        public PluginWatcher(FileSystemWatcher watcher, IHubContext<NotificationsHub, INotificationsHub> hubContext)
        {
            _watcher = watcher;
            _hubContext = hubContext;
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            _watcher.Path = AppDomain.CurrentDomain.BaseDirectory + "/plugins";

            _watcher.NotifyFilter = NotifyFilters.LastAccess
                                    | NotifyFilters.LastWrite
                                    | NotifyFilters.FileName
                                    | NotifyFilters.DirectoryName;

            _watcher.Filter = "*.dll";

            Observable
                .FromEventPattern<FileSystemEventHandler, FileSystemEventArgs>(
                    h => _watcher.Created += h,
                    h => _watcher.Created -= h)
                .Select(x => x.EventArgs)
                .Select(t => t.Name)
                .Subscribe(c => 
                {
                    Console.WriteLine(c);
                    try
                    {
                        var assembly = Assembly.LoadFile(AppDomain.CurrentDomain.BaseDirectory + "/plugins/" + c);
                        var pluginUploadedEvent = new PluginUploadedEvent(assembly.GetName());
                        _hubContext.Clients.All.OnPluginUploaded(pluginUploadedEvent);
                    }
                    catch
                    {

                    }
                });

            //CreatedFiles
            //        .Select(t => t.Name)
            //        .Subscribe(Console.WriteLine);

            _watcher.EnableRaisingEvents = true;
        }

        public override void Dispose()
        {
            _watcher.Dispose();
        }

        public IObservable<ErrorEventArgs> Errors { get; private set; }

        public IObservable<FileSystemEventArgs> CreatedFiles { get; private set; }
    }
}
