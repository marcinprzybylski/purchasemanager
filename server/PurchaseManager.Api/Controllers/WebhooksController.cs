﻿using AppVeyor.Model;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace PurchaseManager.Api.Controllers
{
    [Route("appveyor/webhook")]
    public class WebhooksController : Controller
    {
        // https://www.appveyor.com/docs/notifications/#webhooks
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] AppVeyorWebhookPayload appVeyorWebhookPayload)
        {

            return Ok(appVeyorWebhookPayload);
        }

        // https://docs.travis-ci.com/user/notifications


    }
}
