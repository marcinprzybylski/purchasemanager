﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace PurchaseManager.Api.Controllers
{
    [Route("api/[controller]")]
    public class ConfigController : Controller
    {
        [HttpGet]
        public ActionResult<List<dynamic>> GetAll()
        {
            return new List<dynamic>
            {
                new
                {
                    Message = "Testing controller"
                }
            };
        }
    }
}
