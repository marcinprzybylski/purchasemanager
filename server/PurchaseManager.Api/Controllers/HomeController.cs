﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using PurchaseManager.Api.Hubs;
using System.Threading.Tasks;

namespace PurchaseManager.Api.Controllers
{
    [Route("api/[controller]")]
    public class HomeController : Controller
    {
        private readonly IHubContext<NotificationsHub> _notificationsHub;

        public HomeController(IHubContext<NotificationsHub> notificationsHub)
        {
            _notificationsHub = notificationsHub;
        }

        [HttpPost]
        public async Task<IActionResult> Post()
        {
            await _notificationsHub.Clients.All.SendAsync("Notify", "Home page loaded");
            return Ok();
        }
    }
}
