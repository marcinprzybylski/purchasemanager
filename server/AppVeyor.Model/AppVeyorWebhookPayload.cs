﻿using System;

namespace AppVeyor.Model
{
    public class AppVeyorWebhookPayload
    {
        public string EventName { get; set; }

        public AppVeyorWebhookPayloadData EventData { get; set; }
    }

    public class AppVeyorWebhookPayloadData
    {
        public bool Passed { get; set; }

        public string Status { get; set; }

        public DateTime Started {get;set;}

        public DateTime Finished { get; set; }
    }
}
