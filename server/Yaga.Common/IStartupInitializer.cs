﻿namespace Yaga.Common
{
    using System.Linq;

    public interface IStartupInitializer : IInitializer
    {
        void AddInitializer(IInitializer initializer);
    }
}
