﻿namespace Yaga.Common.Mvc
{
    using System;

    public class ServiceId : IServiceId
    {
        private static readonly string UniqueId = $"{Guid.NewGuid():N}";
        public string Id => UniqueId;
    }
}
