﻿namespace Yaga.Common.Mvc
{
    public interface IServiceId
    {
        string Id { get; }
    }
}
