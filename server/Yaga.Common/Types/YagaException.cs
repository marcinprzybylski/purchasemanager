﻿namespace Yaga.Common.Types
{
    using System;

    public class YagaException : Exception
    {
        public string Code { get; }

        public YagaException()
        {
        }

        public YagaException(string code)
        {
            Code = code;
        }

        public YagaException(string message, params object[] args)
            : this(string.Empty, message, args)
        {
        }

        public YagaException(string code, string message, params object[] args)
            : this(null, code, message, args)
        {
        }

        public YagaException(Exception innerException, string message, params object[] args)
            : this(innerException, string.Empty, message, args)
        {
        }

        public YagaException(Exception innerException, string code, string message, params object[] args)
            : base(string.Format(message, args), innerException)
        {
            Code = code;
        }
    }
}
