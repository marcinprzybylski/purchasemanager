﻿using System.Threading.Tasks;

namespace Yaga.Common
{
    public interface IInitializer
    {
        Task InitializeAsync();
    }
}
